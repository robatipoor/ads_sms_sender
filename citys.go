package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
	"strings"
)

var citys = map[string]string{"nurabad": "نورآباد", "dorud": "دورود", "saveh": "ساوه", "qaem-shahr": "قائم‌شهر", "babol": "بابل", "amol": "آمل", "lahijan": "لاهیجان", "qeshm": "قشم", "khorramabad": "خرم‌آباد", "karaj": "کرج", "ardabil": "اردبیل", "tabriz": "تبریز", "urmia": "ارومیه", "bushehr": "بوشهر", "shahrekord": "شهرکرد", "shiraz": "شیراز", "rasht": "رشت", "gorgan": "گرگان", "hamadan": "همدان", "bandar-abbas": "بندرعباس", "ilam": "ایلام", "isfahan": "اصفهان", "kerman": "کرمان", "kermanshah": "کرمانشاه", "bojnourd": "بجنورد", "mashhad": "مشهد", "birjand": "بیرجند", "ahvaz": "اهواز", "yasuj": "یاسوج", "sanandaj": "سنندج", "arak": "اراک", "sari": "ساری", "qazvin": "قزوین", "qom": "قم", "semnan": "سمنان", "zahedan": "زاهدان", "yazd": "یزد", "zanjan": "زنجان", "tehran": "تهران", "zabol": "زابل", "baneh": "بانه"}

// GetFarsiNameCity get farsi name city
func GetFarsiNameCity(city string) string {
	if v, ok := citys[city]; ok {
		return v
	}
	log.Fatal("City Name Invalid")
	return ""
}

// GetAllCity get all city name
func GetAllCity() []string {
	s := make([]string, 1)
	for k := range citys {
		s = append(s, k)
	}
	return s
}

// GetLastPostDate get lastPostDate
func GetLastPostDate(city string) int {
	url := fmt.Sprintf("https://divar.ir/%s/%s/browse", city, GetFarsiNameCity(city))
	resp, err := http.Get(url)
	if err != nil {
		log.Println(err)
		return 0
	}
	defer resp.Body.Close()
	b, err := ioutil.ReadAll(resp.Body)
	s := string(b)
	slice := strings.Split(s, "lastPostDate")
	if len(slice) >= 2 {
		i, err := strconv.Atoi(slice[1][3:18])
		if err != nil {
			log.Println(err)
			return 0
		}
		return i
	}
	return 0
}
