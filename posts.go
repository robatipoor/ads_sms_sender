package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
	"time"
)

// JSONResult struct type
type JSONResult struct {
	Result `json:"result"`
}

// Result struct type
type Result struct {
	PostList []Post `json:"post_list"`
}

// Post struct type
type Post struct {
	Desc  string `json:"desc"`
	Token string `json:"token"`
	Title string `json:"title"`
}

// SearchNewPost search new post form divar site
func SearchNewPost(posts chan<- Post) {
	go func() {
		var choice string
		var allCitys = GetAllCity()
		if ArrayContains(allCitys, *city) {
			choice = *city
		} else {
			choice = allCitys[CreateRandomNumber(1, uint32(len(citys)))]
		}
		datePost := GetLastPostDate(choice)
		for {
			s := fmt.Sprintf(`{"jsonrpc":"2.0","id":1,"method":"getPostList","params":[[["place2",0,["13"]]],%d]}`, datePost)
			datePost -= CreateRandomNumber(1, 300)
			url := "https://search.divar.ir/json/"
			b := []byte(s)
			req, err := http.NewRequest("POST", url, bytes.NewBuffer(b))
			if err != nil {
				log.Println(err)
				return
			}
			req.Header.Set("Content-Type", "application/json")
			req.AddCookie(&http.Cookie{
				Name:  "city",
				Value: choice,
			})
			cli := http.Client{}
			resp, err := cli.Do(req)
			if err != nil {
				log.Println(err)
				return
			}
			defer resp.Body.Close()
			buf, err := ioutil.ReadAll(resp.Body)
			if err != nil {
				log.Println(err)
				return
			}
			var j JSONResult
			json.Unmarshal(buf, &j)
			for _, p := range j.PostList {
				posts <- p
			}
		}
	}()
}

// CreateRandomNumber create random number
func CreateRandomNumber(min, max uint32) int {
	rand.Seed(time.Now().UnixNano())
	return rand.Intn(int((max - min) + min))
}

// ArrayContains check value
func ArrayContains(arr []string, i string) bool {
	if i == "" {
		return false
	}
	for _, a := range arr {
		if a == i {
			return true
		}
	}
	return false
}
