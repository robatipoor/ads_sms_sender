package main

import (
	"fmt"
	"log"

	kavenegar "github.com/kavenegar/kavenegar-go"
)
// SendMessage send sms message to receptor
func SendMessage(ads <-chan Advertise) {
	ignoreList := GetIginoreListNumbers()
	for a := range ads {
		if _, ok := ignoreList[a.Number]; !ok {
			fmt.Println("Send Message to ", a.Number)
			api := kavenegar.New(apiKey)
			sender := ""
			receptor := []string{a.Number}
			if res, err := api.Message.Send(sender, receptor, *message, nil); err != nil {
				switch err := err.(type) {
				case *kavenegar.APIError:
					log.Println(err.Error())
				case *kavenegar.HTTPError:
					log.Println(err.Error())
				default:
					log.Println(err.Error())
				}
			} else {
				for _, r := range res {
					log.Println(r.Status)
					Set(a)
				}
			}
		}
	}
}
// GetIginoreListNumbers get ignore list number
func GetIginoreListNumbers() map[string]struct{} {
	sslice := client.Keys("*")
	m := make(map[string]struct{})
	for _, num := range sslice.Val() {
		m[num] = struct{}{}
	}
	return m
}
// Set func set advertise to redis db
func Set(ads Advertise) {
	a, err := ads.Marshal()
	if err != nil {
		log.Println(err)
		return
	}
	status := client.Set(ads.Number, string(a), 0)
	if status.Val() == "OK" {
		fmt.Println("Store Number Phone ", ads.Number)
	} else {
		fmt.Println("Unsuccessful Store Number Phone")
	}
}
