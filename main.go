package main

import (
	"encoding/json"
	"flag"
	"log"
	"os"

	"github.com/go-redis/redis"
	"github.com/joho/godotenv"
)
// Advertise store in redis
type Advertise struct {
	Number string
	Title  string
	Desc   string
}
// NewAdvertise create new Advertise
func NewAdvertise() Advertise {
	return Advertise{}
}
// Marshal Advertise to byte
func (a *Advertise) Marshal() ([]byte, error) {
	return json.Marshal(a)
}
// UnMarshal Advertise
func (a *Advertise) UnMarshal(data []byte) error {
	return json.Unmarshal(data, a)
}

var client *redis.Client
var addr string
var password string
var apiKey string
var message *string
var city *string

func init() {
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}
	if a := os.Getenv("REDIS_ADDR"); a == "" {
		addr = "localhost:6379"
	} else {
		addr = a
	}
	if p := os.Getenv("REDIS_PASSWORD"); p == "" {
		password = ""
	} else {
		password = p
	}
	if a := os.Getenv("KAVEHNEGAR_API_KEY"); a == "" {
		log.Fatal("Please Set KAVEHNEGAR_API_KEY https://kavenegar.com/ !!! ")
	} else {
		apiKey = a
	}
	client = redis.NewClient(&redis.Options{
		Addr:     addr,
		Password: password,
		DB:       0,
	})
	message = flag.String("message", "", "sms message body")
	city = flag.String("province", "", "iran province name")
	flag.String("version", "", "Version 0.0.1")
	flag.Parse()
}

func main() {
	posts := make(chan Post, 100)
	ads := make(chan Advertise, 100)
	SearchNewPost(posts)
	GetContactPhoneNumber(posts, ads)
	SendMessage(ads)
}
