package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"time"
)
// Response struct type
type Response struct {
	Widgets `json:"widgets"`
	Token   string `json:"token"`
}
// Widgets struct type
type Widgets struct {
	Contact `json:"contact"`
}
// Contact struct type 
type Contact struct {
	Phone      string `json:"phone"`
	Chat       bool   `json:"chat"`
	IsGoodTime bool   `json:"is_good_time"`
}
// NewResponse create new response
func NewResponse() *Response {
	return &Response{}
}
// Marshal Response to byte
func (r *Response) Marshal() ([]byte, error) {
	return json.Marshal(r)
}
// UnMarshal byte to response 
func (r *Response) UnMarshal(b []byte) error {
	err := json.Unmarshal(b, r)
	if err != nil {
		return err
	}
	return nil
}
// GetContactPhoneNumber get contant phone number 
func GetContactPhoneNumber(posts <-chan Post, ads chan<- Advertise) {
	go func() {
		for {
			time.Sleep(time.Second)
			post := <-posts
			response, err := http.Get(fmt.Sprintf("https://api.divar.ir/v5/posts/%s/contact/", post.Token))
			if err != nil {
				log.Println(err)
				return
			}
			defer response.Body.Close()
			buf, err := ioutil.ReadAll(response.Body)
			if err != nil {
				log.Println(err)
				return
			}
			resp := NewResponse()
			err = resp.UnMarshal(buf)
			if err != nil {
				log.Println(err)
				return
			}
			ads <- Advertise{
				Number: resp.Contact.Phone,
				Desc:   post.Desc,
				Title:  post.Title,
			}
		}
	}()
}
